<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\UsesUuid;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'name', 
        'email', 
        'password',
        'role_id',
        'photo_profile',
    ];
    protected $primarYKey = "id";

    use UsesUuid;

    public function role(){
        return $this->belongsTo('App\User', 'role_id');
    }
}

