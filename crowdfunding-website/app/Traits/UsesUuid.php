<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait UsesUuid{
    // protected $keyType = "string";
    // public $incrementing = false;

    public function getIncrementing(){
        return false;
    }

    public function getKeyType(){
        return "string";
    }

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}

?>