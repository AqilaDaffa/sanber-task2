<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Role extends Model
{
    protected $fillable = ["name"];
    protected $primarYKey = "id";
    
    use UsesUuid;

    public function user(){
        return $this->hasMany('App\User', 'role_id');
    }
}
  